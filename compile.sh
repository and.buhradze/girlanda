echo $1
rm output.bin output.hex
avr-gcc -Wall -g -Os -mmcu=attiny13 -o output.bin $1
avr-size output.bin
avr-objcopy -O ihex output.bin output.hex