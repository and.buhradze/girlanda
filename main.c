/**
 * Copyright (c) 2019, Łukasz Marcin Podkalicki <lpodkalicki@gmail.com>
 * ATtiny13/031
 * Example of hardware Fast PWM.
 * Features:
 * - PWM duty resolution: 0..255
 * - PWM frequencies (@1.2MHz): 4.687kHz, 585Hz, 73Hz, 18Hz, 4Hz 
 */
#define F_CPU 960000 //CPU frequency define

#include <avr/io.h> //import io, delay and interrupt headers
#include <util/delay.h>
#include <avr/interrupt.h>



#define N_1    (_BV(CS00))
#define N_8    (_BV(CS01))
#define N_64   (_BV(CS01)|_BV(CS00))
#define N_256  (_BV(CS02))
#define N_1024 (_BV(CS02)|_BV(CS00))

static void
pwm_init(void)
{
    DDRB |= _BV(PB0); // set PWM pin as OUTPUT
    TCCR0A |= _BV(WGM01)|_BV(WGM00); // set timer mode to FAST PWM
    TCCR0A |= _BV(COM0A1); // connect PWM signal to pin (AC0A => PB0)
}

/* When timer is set to Fast PWM Mode, the freqency can be
calculated using equation: F = F_CPU / (N * 256) 
Posible frequencies (@1.2MHz):
 -> F(N_1) = 4.687kHz
 -> F(N_8) = 585Hz
 -> F(N_64) = 73Hz
 -> F(N_256) = 18Hz
 -> F(N_1024) = 4Hz */
static void
pwm_set_frequency(uint32_t N)
{

    TCCR0B = (TCCR0B & ~((1<<CS02)|(1<<CS01)|(1<<CS00))) | N; // set prescaler
}

static void
pwm_set_duty(uint8_t duty)
{

    OCR0A = duty; // set the OCRnx
}

static void
pwm_stop(void)
{

    TCCR0B &= ~((1<<CS02)|(1<<CS01)|(1<<CS00)); // stop the timer
}



ISR(INT0_vect) //обработка прерываний INT0
{


    change_taps();
    GIFR=0xFF;
           
}

volatile int delay_time = 4;

volatile int count_of_button_taps = 0;

void change_taps() {
    
    switch (count_of_button_taps)
    {
    case 0:
        delay_ms(100);
        count_of_button_taps = 1;
        blink_several_times(count_of_button_taps);
        break;
    case 1:
        delay_ms(100);
        delay_time += 3;
        count_of_button_taps = 2;
        blink_several_times(count_of_button_taps);
        break;
    case 2:
        delay_ms(100);
        delay_time += 3;
        count_of_button_taps = 3;
        blink_several_times(count_of_button_taps);
        break;
    case 3:
        delay_ms(100);
        delay_time += 3;
        count_of_button_taps = 4;
        blink_several_times(count_of_button_taps);
        break;
    case 4:
        delay_ms(100);
        delay_time += 3;
        
        count_of_button_taps = 5;
        blink_several_times(count_of_button_taps);
        break;
    case 5:
        delay_ms(200);
        delay_time = 4;
        count_of_button_taps = 0;
        
        break;
    }
    
}

int delay_ms(int n) //dynamic delay function with ability to use variable
{
    while (n--)
    {
        _delay_ms(1);
    }
}

int blink_several_times(int n) //function for blinking
{
    while (n--)
     {
         PORTB |= (1 << PB2); //set high debug LED during interrupt
         delay_ms(200);
         PORTB &= ~(1 << PB2);
         delay_ms(200);
     }
}
int
main(void)
{
    


    /* setup */
    pwm_init();
    pwm_set_frequency(N_1);

    DDRB &= ~(1 << PB1); //PB1 as input
    PORTB |= (1 << PB1); //PB1 pull-UP

    GIMSK = 0b01000000; //INT0 enable interrupt
    MCUCR = 0b00000011; //INT0 rising fall interrupt type

    
    DDRB |= (1<<PB2); //debug led set to output
    PORTB &= ~(1<<PB2); //debug let set to LOW
    
    

    sei(); //разрешение прерываний
    
    /* loop */
    while (1) {
        if (count_of_button_taps == 0) {
            pwm_set_duty(255);
        }
        else {
            for (int i=0;i<=255;i++) {
            pwm_set_duty(i);
            delay_ms(delay_time);
        }
        for (int i=255;i>=0;i--) {
            pwm_set_duty(i);
            delay_ms(delay_time);
        }
        }
        
        
    }
}