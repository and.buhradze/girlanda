@echo off
del %~dp0\output.bin %~dp0\output.hex

set toolchain="C:\Users\andbu\Documents\pet_proj\avr-toolchain"



%toolchain%\bin\avr-gcc.exe -Wall -g -Os -mmcu=attiny13a -o %~dp0\output.bin %~dp0\main.c
%toolchain%\bin\avr-size.exe %~dp0\output.bin
%toolchain%\bin\avr-objcopy.exe -O ihex %~dp0\output.bin %~dp0\output.hex