
#define F_CPU 960000L
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>




ISR (INT0_vect)
{
    

    
    // pin is high
    PORTB |= (1<<PB0);
    _delay_ms(400);
    PORTB &= ~(1<<PB0);
    _delay_ms(400);
    
    GIFR=0xFF;
    
    
    
}



int main()

{
    
    DDRB |= (1<<PB0); //set pb0 to output and LOW level
    PORTB &= ~(1<<PB0);
    
    GIMSK = 0b01000000; //enable INT0 interrupts
    MCUCR = 0b00000011; //rising edge INT0 interrupt mode

    
    DDRB &= ~(1<<PB1); //set pb1 to input and pull-up it
    PORTB |= (1<<PB1);

    sei();
    while(1) {
        
    } 

}